import TimeTracker from '../helper/timeTracker/timeTracker';
import type { IUser } from './user';

export interface ITimeTrackerRouter {
  user:IUser
  timeTracker: TimeTracker
}
