import type { ITaskCompleted } from '../../interfaces/taskCompleted';

class TimeTracker {
  private active : boolean;

  private startTime : number;

  private endTime:number;

  private allTasks: ITaskCompleted[];

  name: string;

  description:string;

  constructor() {
    this.active = false;
    this.startTime = 0;
    this.endTime = 0;
    this.allTasks = [];
    this.name = '';
    this.description = '';
  }

  startTimer(name:string, description:string): void {
    if (this.active) {
      return;
    }

    this.name = name;
    this.description = description;
    this.active = true;
    this.startTime = Date.now();
  }

  stopTimer(): void {
    if (!this.active) {
      return;
    }

    this.active = false;
    this.endTime = Date.now();
    this.allTasks.push({
      name: this.name,
      description: this.description,
      startTime: this.startTime,
      endTime: this.endTime,
      completedDate: new Date(),
    });
  }

  getTasksCompleted() : ITaskCompleted[] {
    return this.allTasks;
  }
}

export default TimeTracker;
