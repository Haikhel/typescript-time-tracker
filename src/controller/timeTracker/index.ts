import * as express from 'express';
import TimeTracker from '../../helper/timeTracker/timeTracker';
import type { IStartTask } from '../../interfaces/startTask';
import type { ITimeTrackerRouter } from '../../interfaces/timeTrackerRouter';

class TimeTrackerController {
  public path = '/tracker';

  public router = express.Router();

  private usersSession: ITimeTrackerRouter[] = [];

  constructor() {
    this.intializeRoutes();
  }

  public intializeRoutes() {
    this.router.get(`${this.path}/:id`, this.getAllTrackers);
    this.router.post(`${this.path}/start/:id`, this.startTask);
    this.router.post(`${this.path}/stop/:id`, this.stopTask);
  }

  getAllTrackers = (request: express.Request, response: express.Response) => {
    const currentUser = this.usersSession.find((user) => user.user.id === request.params.id);
    if (currentUser) {
      response.send(currentUser?.timeTracker.getTasksCompleted());
      return;
    }
    response.send({ message: 'user not found' });
  };

  startTask = (request: express.Request, response: express.Response) => {
    if (!request.params.id) {
      response.send({ message: 'user not found' });
      return;
    }

    const trackInfo: IStartTask = request.body;

    let currentUser = this.usersSession.find((user) => user.user.id === request.params.id);

    if (!currentUser) {
      currentUser = {
        user: {
          id: request.params.id,
        },
        timeTracker: new TimeTracker(),
      };
      this.usersSession.push(currentUser);
    }

    currentUser.timeTracker.startTimer(trackInfo.name, trackInfo.description);

    response.send({ message: 'success', userId: currentUser.user.id });
  };

  stopTask = (request:express.Request, response:express.Response) => {
    if (!request.params.id) {
      response.send({ message: 'user not found' });
      return;
    }

    const currentUser = this.usersSession.find((user) => user.user.id === request.params.id);

    if (!currentUser) {
      response.send('user not found');
      return;
    }

    currentUser.timeTracker.stopTimer();

    response.send({ message: 'success' });
  };
}

export default TimeTrackerController;
