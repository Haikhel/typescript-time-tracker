export interface ITaskCompleted {
  startTime:number
  endTime:number
  description:string
  name:string
  completedDate: Date
}
