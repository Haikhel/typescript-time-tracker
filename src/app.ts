import * as bodyParser from 'body-parser';
import express, { RequestHandler } from 'express';
import swaggerUi from 'swagger-ui-express';
import cors from 'cors';
import * as swaggerDocument from './swagger/openapi.json';

class App {
  public app: express.Application;

  public port: number;

  constructor(controllers:any, port:number) {
    this.app = express();
    this.port = port;

    this.initializeMiddlewares();
    this.initializeControllers(controllers);
  }

  private initializeMiddlewares() {
    this.app.use(bodyParser.json());
    this.app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
    this.app.use(cors() as RequestHandler);
  }

  private initializeControllers(controllers:any) {
    controllers.forEach((controller:any) => {
      this.app.use('/', controller.router);
    });
  }

  public listen() {
    this.app.listen(this.port, () => {
      console.log(`App listening on the port ${this.port}`);
    });
  }
}

export default App;
