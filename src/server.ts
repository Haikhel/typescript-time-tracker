import App from './app';
import TimeTrackerController from './controller/timeTracker/index';
import config from './config';

const app = new App(
  [
    new TimeTrackerController(),
  ],
  config.port,
);
app.listen();
