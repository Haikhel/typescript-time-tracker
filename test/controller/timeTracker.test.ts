import axios from 'axios';
import App from '../../src/app';
import config from '../../src/config';
import TimeTrackerController from '../../src/controller/timeTracker/index';

test('time tracker controller', async () => {
  const app = new App(
    [
      new TimeTrackerController(),
    ],
    config.port,
  );

  app.listen();

  const start = await axios.post('http://localhost:5000/tracker/start/1', {
    name: 'test',
    description: '123',
  });

  expect(start.data).toEqual({ message: 'success', userId: '1' });

  const stop = await axios.post('http://localhost:5000/tracker/stop/1');

  expect(stop.data).toEqual({ message: 'success' });

  const get = await axios.get('http://localhost:5000/tracker/1');

  expect(get.data[0].description).toBeTruthy();
});
